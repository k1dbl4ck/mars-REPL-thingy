'use strict';

let chai = require('chai');
let assert = chai.assert;
let nav = require('../roverNavigation');

describe('Rover Navigation', function() {
    it('should return false if no value is passed', function() {
        assert.equal(nav(), false);
    });

    it('should return false if something other than an array is passed', function() {
        assert.equal(nav("test"), false);
    });

    it('should return false if array is corrupted', function() {
        assert.equal(nav(["88", "1   E", "M MLM  RMMRRMML"]), false);
    });

    it('should return 3 3 S if a value of ["8 8", "1 2 E", "MMLMRMMRRMML"] is passed', function() {
        assert.equal(nav(["8 8", "1 2 E", "MMLMRMMRRMML"]), "3 3 S");
    });
});