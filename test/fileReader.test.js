'use strict';

let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;

let reader = require('../fileReader');

describe('File Reader', function() {
    it('should return false if no value is passed', function() {
        assert.equal(reader(), false);
    });

    it('should return false if file does not exist', function() {
        assert.equal(reader('someFooFile.txt'), false);
    });

    it('should return an array of navigation data if valid file name is passed', function() {
        expect(reader('testNavigationFile.txt')).to.be.a('array');
    });


});