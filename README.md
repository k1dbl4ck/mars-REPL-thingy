### DocFox Assignment ###


#### Note :
So, to be honest, when I got this assignment.. uhm.. Greg and the man in the button shirt, I momentarily imagined myself adjusting my glasses, shuffling over 
to my immaculate desk with a glass of water, and spending the next 2 hours coming up with my idea of the perfect algo for this whilst my Maine Coon cat lay curled 
up on some trendy chaise.

But then I realised that would just be silly.

In the real world you get a problem at 20:33 in the evening, and in the midst of everything else going on, you have to ship a commit. 
So you open your laptop and after a bit of fiddling you go :

http://lmgtfy.com/?q=thoughtworks+mars+rover+solution

And out pops a piece (well.. a gist) of C#...

https://gist.github.com/haneytron/efd06d4a4bcb543793ece855f301dd48

So I think.. ok.. My entire stack runs on containerised Nodejs apps, scaled by Redis, and fed by a MongoDB cluster.. 

I have not C sharped in ages... I could just fork it, slap it in VS and give you the solution... But no.. I'll port it to javascript and give you one line execution. 

Some Thoughts : 
- Used ```feat/xxxx``` branch convention before merge to master (like in the real world) 
- Has Tests ( Although I didn't TDD this, the tests came last )  
- Uses basic ES6 - albeit static (const) based modules - I can obviously dev in full TypeScript and ES6
- My choice in GitLab was arbitary. Github is still the best. 
- roverNavigation.js on its own could easily be converted to be used as a lambda function (FaaS) 
- fileReader is seperate so roverNavigation can be used as its own module to plug into an API or something
- I assumed bounds are hard and commands to move outside will not throw an error
- Although I used the C# gist as a starting point, I added some error handling and checks. 
- In the real world I would not just return false, but rather a response object containing a status, message, etc. 
- Although my markdown in this file is basic, I know what it is and how to use it
- I could have come up with a similar algo without the C# reference, but re-inventing wheels is not my thing, I rather mould them to suit my requirements. 
 

#### Requirements : 
- NodeJS (version 6.10.0+) installed and in your path - https://nodejs.org
- NPM (Node Package Manager) installed and in your path

#### Install : 
1. Clone / Fork the repository
2. Inside the repo run : ```npm install``` to install dependencies (In this case just for tests)
3. Optionally run ```npm install -g mocha``` to install the global test framework

#### Run : 

```npm start testNavigationFile.txt```

#### Test : 

```npm test```






