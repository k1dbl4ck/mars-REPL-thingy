let fs = require('fs');

const fileReader = (fileName) => {

    if (!fileName || !fs.existsSync(fileName)) {
        return false;
    }

    let raw = fs.readFileSync(fileName, 'utf8');
    raw = raw.split("\n");

    let data = [];

    for (var i in raw) {
        if (raw[i] != '') { data.push(raw[i]); }
    }

    return data;

}

module.exports = fileReader;