let reader = require('./fileReader');
let nav = require('./roverNavigation');

let fileName = process.argv[2];

let data = reader(fileName);

if (!data) {
    console.info("Please enter a valid filename - e.g  npm start myRoverNavigationFile.txt");
} else {

    let result = nav(data);
    console.log("Rover end location is : ", result);

}