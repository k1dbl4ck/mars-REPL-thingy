const roverNavigation = (args) => {

    try {

        if (!args || typeof args == 'string' || args.constructor !== Array) {
            return false;
        }

        let data = args;
        let directions = 'NESW';
        let bounds = data[0].split(' ');

        let vectors = [
            [0, 1],
            [1, 0],
            [0, -1],
            [-1, 0]
        ];

        let start = data[1].split(' ');
        let x = start[0];
        let y = start[1];
        let d = directions.indexOf(start[2]);

        let commands = (data[2] + "").split("");

        for (var command in commands) {

            switch (commands[command]) {
                case 'L':
                    d = (--d + vectors.length) % vectors.length;
                    break;
                case 'R':
                    d = ++d % vectors.length;
                    break;
                default:
                    x = Math.min(parseInt(bounds[0]), Math.max(0, parseInt(x) + parseInt(vectors[d][0])));
                    y = Math.min(parseInt(bounds[1]), Math.max(0, parseInt(y) + parseInt(vectors[d][1])));
                    break;
            }

        }

        if (isNaN(x) || isNaN(y)) {
            return false;
        }

        return [x, y, directions[d]].join(' ');

    } catch (e) {
        console.error('-->' + e);
        return false;
    }



}


module.exports = roverNavigation;